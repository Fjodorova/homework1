FROM python:3

RUN pip install pystrich && \
    pip install bs4 

EXPOSE 8080

ADD HoeWarmIsHetInDelft.py /

CMD [ "python", "./HoeWarmIsHetInDelft.py" ]