#!/usr/bin/python

from urllib.request import urlopen
from bs4 import BeautifulSoup

url = "https://www.weerindelft.nl"
page = urlopen(url)

soup = BeautifulSoup(page.read(), "html.parser")
print(soup.prettify())
nowTemp = soup.findAll (attrs={"class":"ajaxtemp.ajax"})
print (nowTemp, "degrees Celsius")